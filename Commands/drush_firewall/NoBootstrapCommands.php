<?php

declare(strict_types=1);

namespace Drush\Commands\drush_firewall;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\AnnotatedCommand\Hooks\HookManager;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Drush\Commands\sql\SqlSyncCommands;

/**
 * Commands here run without Drupal bootstrap.
 */
class NoBootstrapCommands extends DrushCommands {

  /**
   * Adds an option to disable the firewall.
   */
  #[CLI\Hook(type: HookManager::OPTION_HOOK, target: '*')]
  #[CLI\Option(name: 'disable-firewall', description: 'Disables the firewall and allows commands to run.')]
  public function addDisableOptionToCommands($options = ['disable-firewall' => FALSE]): void {}

  /**
   * Prevent overwriting the production database.
   *
   * @throws \Exception
   */
  #[CLI\Hook(type: HookManager::ARGUMENT_VALIDATOR, target: SqlSyncCommands::SYNC)]
  public function sqlSyncValidate(CommandData $commandData) {
    $target = $commandData->input()->getArgument('target');
    if (strpos($target, 'prod') !== FALSE) {
      throw new \Exception(dt('Overwriting production database is disabled.'));
    }
  }

}
