<?php

declare(strict_types=1);

namespace Drupal\drush_firewall\Drush\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\AnnotatedCommand\CommandResult;
use Consolidation\AnnotatedCommand\Hooks\HookManager;
use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\Site\Settings;
use Drupal\Core\State\StateInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;

/**
 * Firewall commands for Drush.
 */
class FirewallDrushCommands extends DrushCommands {

  use AutowireTrait;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new instance of FirewallCommands.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(StateInterface $state) {
    parent::__construct();
    $this->state = $state;
  }

  /**
   * Adds an option to disable the firewall.
   *
   * Note this only works on bootstrapped commands. It as also been added with
   * non-bootstrap support (check README).
   *
   * @see \Drush\Commands\drush_firewall\NoBootstrapCommands::addDisableOptionToCommands()
   */
  #[CLI\Hook(type: HookManager::OPTION_HOOK, target: '*')]
  #[CLI\Option(name: 'disable-firewall', description: 'Disables the firewall and allows commands to run.')]
  public function addDisableOptionToCommands($options = ['disable-firewall' => FALSE]): void {}

  /**
   * Pre Command hook.
   *
   * This is the main hook to detect and control access to commands based on
   * the environment's settings.
   *
   * @param \Consolidation\AnnotatedCommand\CommandData $commandData
   *   The command data.
   *
   * @return \Consolidation\AnnotatedCommand\CommandResult|null
   *   Returns null if the command is able to run, Otherwise returns a
   *   CommandResult with exit code 0.
   */
  #[CLI\Hook(type: HookManager::PRE_COMMAND_HOOK, target: '*')]
  public function firewallPolicies(CommandData $commandData): ?CommandResult {
    $command = $commandData->annotationData()->get('command');

    // Firewall has been specifically disabled, allow any command to run.
    if (!$commandData->input()->hasOption('disable-firewall') || $commandData->input()->getOption('disable-firewall')) {
      return NULL;
    }

    // Check the denied commands first.
    if ($this->isCommandDenied($command)) {
      return CommandResult::dataWithExitCode(sprintf('The "%s" command is disabled. Use --disable-firewall to run anyway.', $command), 0);
    }

    // Production protection.
    if ($this->isProductionProtected($commandData)) {
      return CommandResult::dataWithExitCode(sprintf('The "%s" command is disabled on production. Use --disable-firewall to run anyway.', $command), 0);
    }

    // Maintenance protection.
    if ($this->isMaintenanceProtected($command)) {
      return CommandResult::dataWithExitCode(sprintf('The "%s" command is disabled in maintenance mode. Use --disable-firewall to run anyway.', $command), 0);
    }

    // Allows the command to run.
    return NULL;
  }

  /**
   * Check if a command is denied.
   *
   * @param string $command
   *   The command to check.
   *
   * @return bool
   *   TRUE if the command is denied, FALSE otherwise.
   */
  protected function isCommandDenied(string $command): bool {
    return \in_array($command, Settings::get('drush_firewall_denied') ?? [], TRUE);
  }

  /**
   * Check if a command is production only.
   *
   * @param \Consolidation\AnnotatedCommand\CommandData $commandData
   *   The command data.
   *
   * @return bool
   *   TRUE if the command is production only, FALSE otherwise.
   */
  protected function isProductionProtected(CommandData $commandData): bool {
    $command = $commandData->annotationData()->get('command');

    // @todo some commands might not have targer but still be needed here.
    if (!$commandData->input()->hasArgument('target')) {
      return FALSE;
    }

    // @todo This is a temporary solution, Might want to check URI for remote.
    $target = $commandData->input()->getArgument('target') ?? '';
    $isProdTarget = strpos($target, 'prod') !== FALSE;
    // @todo currently rsync is the only preventable command here.
    $settings = Settings::get('drush_firewall_production_denied') ?? [
      'sql:drop',
      'sql:santitize',
      'sql:sync',
      'core:rsync',
    ];
    return $isProdTarget && \in_array($command, $settings, TRUE);
  }

  /**
   * Check if a command is allowed in maintenance mode.
   *
   * @param string $command
   *   The command to check.
   *
   * @return bool
   *   TRUE if the command is maintenance only, FALSE otherwise.
   */
  protected function isMaintenanceProtected(string $command): bool {
    // @todo we may want users to control this.
    $alwaysAllowed = [
      'state:set',
      'cache:clear',
      'updatedb',
      'config:import',
      'deploy:hook',
      'deploy:batch-process',
      'core:requirements',
      'maint:set',
      'maint:get',
      'maint:status',
    ];
    $settings = Settings::get('drush_firewall_maintenance_allowed') ?? [];
    $allowedCommands = array_merge($settings, $alwaysAllowed);
    return !\in_array($command, $allowedCommands, TRUE) && $this->state->get('system.maintenance_mode');
  }

}
