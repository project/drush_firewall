# Drush Firewall

Drush Firewall allows disabling commands while a site is in maintenance mode, disabling commands all together and some protection against running productions commands from another source (i.e. sql-sync)

## Configuration

Install the module, and then configure your settings. Generally this is best used per environment-included settings; however, globals can work here as well. The following are available:

- $settings['drush_firewall_denied'] = [];
	- These commands will never be allowed to run.
- $settings['drush_firewall_production_denied'] = [];
	- These commands will be denied if the target alias is "prod"
- $settings['drush_firewall_maintenance_allowed'] = [];
	- These commands will be allowed while the site is in maintenance mode, all other commands (With the exception of necessary - see code) will be denied.

## No Bootstrap Commands
Some commands unfortunately do not bootstrap Drupal (See [[https://www.drush.org/12.x/bootstrap/]]) and therefore cannot be run from the module. There is a special file here for things like `sql:sync` protection. For this to work, you need to add the module to `drush/drush.yml` , for example:

```
drush:  
  include:  
    - '/var/www/docroot/modules/contrib/drush_firewall'
```

## Disable Firewall

In the off chance you need to allow a command that has otherwise been disabled, you can pass along `--disable-firewall` to the command which will disable all checks.